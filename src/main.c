#include <dlfcn.h>
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <alloca.h>

#include <test.h>
#define NB_M 10

m_method_f *init();
void destruct(m_method_f *method);
void *get_handle(void *handle);

void exec(m_method_f *syscall, char *tab)
{
  int i;

  i = -1;
  while(tab[++i] != -1)
    syscall[(int)tab[i]](NULL);
}

int main()
{
  m_method_f *syscall;
  char tab[12] = {2,1,2,1,3,4,5,6,0,-1};

  syscall = init();
  exec(syscall, tab);
  destruct(syscall);
  return (0);
}

m_method_f *init()
{
  m_method_f *syscall;
  char **tab_method;
  void *handle;
  char **(*m_init)();
  int i = -1;

  if (!(handle = dlopen("libtest.so", RTLD_LAZY)))
    {
      printf("An error occured: %s\n", dlerror ());
      return (NULL);
    }
  get_handle(handle);
  if (!(m_init = dlsym(handle, "m_init")))
    {
      printf("An error occured: %s\n", dlerror ());
      dlclose(handle);
      return (NULL);
    }
  tab_method = m_init();
  syscall = (m_method_f *)malloc(sizeof(m_method_f) * NB_M);
  while (tab_method[++i])
    {
      if (!(syscall[i] = dlsym(handle, tab_method[i])))
	{
	  printf("An error occured: %s\n", dlerror ());
	  destruct(syscall);
	  return (NULL);
	}
    }
  syscall[i] = NULL;
  return (syscall);
}

void destruct(m_method_f *method)
{
  dlclose(get_handle(NULL));
  free(method);
}

void *get_handle(void *handle)
{
  static void *svg_handle = NULL;

  if (handle)
    svg_handle = handle;
  return svg_handle;
}
