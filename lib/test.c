#include <test.h>
#include <unistd.h>
#include <string.h>
#include <stdlib.h>
#include <stdio.h>

#define NB_METHOD 10
#define SIZE_METHOD_NAME 10

static char **reg_tab_method(char **tab)
{
  static char **reg = NULL;

  if (tab)
    reg = tab;
  return (reg);
}

char **m_init()
{
  char **tab_method;

  tab_method = (char **)malloc((NB_METHOD + 1) * sizeof(char *));
  tab_method[0] = strdup("m_exit\0");
  tab_method[1] = strdup("m_add_job\0");
  tab_method[2] = strdup("m_add_job_depend\0");
  tab_method[3] = strdup("m_del_job\0");
  tab_method[4] = strdup("m_del_job_depend\0");
  tab_method[5] = strdup("m_run\0");
  tab_method[6] = strdup("m_destroy\0");
  tab_method[7] = NULL;

  reg_tab_method(tab_method);
  return (tab_method);
}

void *m_exit(void *arg)
{
  char **tab_method;
  int i;
  
  printf("m_exit\n");
  tab_method = reg_tab_method(NULL);
  i = -1;
  while (tab_method[++i])
    free(tab_method[i]);
  free(tab_method);
  return (NULL);
}

void *m_add_job(void *arg)
{
  printf("add_job\n");
  return (NULL);
}

void *m_add_job_depend(void *arg)
{
  printf("add_job_depend\n");
  return (NULL);
}
void *m_del_job(void *arg)
{
  printf("del_job\n");
  return (NULL);
}

void *m_del_job_depend(void *arg)
{
  printf("del_job_depend\n");
  return (NULL);
}

void *m_run(void *arg)
{
  printf("run\n");
  return (NULL);
}

void *m_destroy(void *arg)
{
  printf("destroy\n");
  return (NULL);
}
